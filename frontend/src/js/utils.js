export function stringToDate(fecha) {
  return fecha ? new Date(fecha) : null
}

export function fechaToLocaleDateString(fecha) {
  let opciones = { year: 'numeric', month: '2-digit', day: '2-digit' }
  return fecha ? fecha.toLocaleDateString('es-ES', opciones) : null
}

export function addEstadoAsignacion(equipo) {
  let textoEstado = 'Sin Asignar'

  if (equipo.codigoPropietario) {
    textoEstado = 'Asignado'
    if (equipo.tipoEquipo == 'EQUIPO_UNIDAD') {
      textoEstado += ' (Unidad)'
    } else if (equipo.tipoEquipo == 'EQUIPO_PERSONAL') {
      textoEstado += ' (Personal)'
    }
  }

  equipo.estadoAsignacion = textoEstado
}