const textoResultadoPeticion = {
  '200': {
    'default': 'Su acción se ha realizado correctamente',
    'getModelos': null,
    'saveModelo': 'El Modelo se ha guardado correctamente',
    'seleccionaModelo': null,
    'editModelo': 'El Modelo se ha guardado correctamente',
    'removeModelo': 'El Modelo se ha eliminado correctamente',
    'getEquipos': null,
    'seleccionaEquipo': null,
    'saveEquipo': 'El Equipo se ha dado de alta correctamente',
    'removeEquipo': 'El Equipo se ha eliminado del inventario',
    'loginPersona': null,
    'compruebaMiembroGC': null,
    'asignarEquipo': 'El Equipo se ha asignado correctamente',
    'desvincularEquipo': 'El Equipo se ha desvinculado. Puede volver a asignarlo',
    'getIncidencias': null,
    'saveIncidencia': 'La incidencia se ha reportado correctactamente',
    'seleccionaIncidencia': null,
    'editIncidencia': 'Se han guardado los cambios correctamente',
    'patchAsignarResolutor': 'Se ha asignado resolutor',
    'patchResolverIncidencia': 'La incidencia se ha marcado como resuelta',
    'patchCerrarIncidencia': 'La incidencia ha quedado cerrada',
    'getResolutores': null,
    'getPersonal': null
  },
  '300': {
    'default': '¡Error!: el servidor ha cambiado de dirección y no es posible acceder en este momento',
  },
  '400': {
    'default': '¡Error!: el servidor informa que los datos son incorrectos',
    'removeModelo': '¡Error!: no puede eliminarse un modelo del que hay stock',
  },
  '401': {
    'default': '¡Error!: su sesión ha expirado',
  },
  '403': {
    'default': '¡Error!: no tiene permisos para realizar esta acción',
    'loginPersona': null,
  },
  '404': {
    'default': '¡Error!: el recurso solicitado no se encuentra disponible en el servidor',
    'compruebaMiembroGC': null,
  },
  '500': {
    'default': '¡Error!: el servidor ha sufrido un error interno',
  },
  'ERR_NETWORK': {
    'default': '¡Error!: no hay conexión con el servidor',
  },
  'ERR_INTERNAL': {
    'default': '¡Error!: se ha producido un error interno del sistema',
  }
}

const TOAST_LIFE_TIME = 5000
const duracionResultadoPeticion = {
  '200': {
    'saveIncidencia': null,
  },
}

export function getMensajeResultadoPeticion(respuesta, tipoPeticion) {
  let codigoEstado = getCodigo(respuesta)
  let mensaje = '¡¡Respuesta desconocida!!'
  let severidad = 'error'
  let duracion = TOAST_LIFE_TIME
  let resultado

  if (textoResultadoPeticion[codigoEstado]) {
    if (typeof textoResultadoPeticion[codigoEstado][tipoPeticion] != 'undefined'){
      mensaje = textoResultadoPeticion[codigoEstado][tipoPeticion]
    } else {
      mensaje = textoResultadoPeticion[codigoEstado]['default']
    }
    severidad = codigoEstado == '200' ? 'success' : 'error'
  }

  if (duracionResultadoPeticion[codigoEstado]) {
    if (typeof duracionResultadoPeticion[codigoEstado][tipoPeticion] != 'undefined'){
      duracion = duracionResultadoPeticion[codigoEstado][tipoPeticion]
    }
  }

  resultado = { mensaje, severidad, duracion, codigoEstado, respuesta }

  // Mensajes que requieren un formateo específico
  formateaMensaje(resultado, tipoPeticion)

  return resultado
}

function getCodigo(respuesta) {
  let codigo

  if (respuesta.status) {
    codigo = '200'
  } else if (respuesta.response) {
    if (respuesta.response.status < 400) {
      codigo = '300'
    } else if (respuesta.response.status <= 404) {
      codigo = String(respuesta.response.status)
    } else if (respuesta.response.status <= 500) {
      codigo = '400'
    } else if (respuesta.response.status < 600) {
      codigo = '500'
    }
  } else if (respuesta.request) {
    codigo = 'ERR_NETWORK'
  } else {
    codigo = 'ERR_INTERNAL'
  }

  return codigo
}

function formateaMensaje(resultado, tipoPeticion) {

  if ((resultado.codigoEstado == '200') && (tipoPeticion == 'saveIncidencia')) {
    resultado.mensaje += ' con código: ' + resultado.respuesta.data.codigo
  }
}