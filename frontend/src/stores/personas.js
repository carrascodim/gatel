import { defineStore } from 'pinia'
import { getMensajeResultadoPeticion } from '@/js/errors'
import { loginUsuarioApi, getPersonaLogueadaApi, getResolutoresApi, getPersonalApi, getPersonaApi, getPersonaPorTipApi } from '@/stores/api-service'
import { useModelosStore } from '@/stores/modelos'
import { useEquiposStore } from '@/stores/equipos'
import { useIncidenciasStore } from '@/stores/incidencias'
import { useUnidadesStore } from '@/stores/unidades'
import jwtDecode from 'jwt-decode'
import { Constantes } from '@/js/Constantes.js'

export const usePersonasStore = defineStore('persona', {
  state: () => {
    let personaGuardada = localStorage.getItem('personaLogueada') ? JSON.parse(localStorage.getItem('personaLogueada')) : null
    let tokenGuardado = localStorage.getItem('token')

    if (personaGuardada && tokenGuardado) {
      let tokenDecodificado = jwtDecode(tokenGuardado)
      if ((new Date() > new Date(tokenDecodificado.exp * 1000)) || (personaGuardada.nombreUsuario != tokenDecodificado.sub)) {
        personaGuardada = null
      }
    } else {
      personaGuardada = null
    }

    if (!personaGuardada) {
      localStorage.removeItem('token')
      localStorage.removeItem('personaLogueada')
    }

    return {
      queriesSinRespuesta: 0,
      erroresPersonasStore: false,
      listaPersonas: [],
      listaResolutores: [],
      listaPersonal: [],
      personaLogueada: personaGuardada,
      personaSeleccionada: null,
      resultadoPeticionPersonas: null,
    }
  },
  getters: {
    loadingPersonasStore() {
      return this.queriesSinRespuesta > 0
    }
  },
  actions: {
    loginPersona(credenciales) {
      let tipoPeticion = 'loginPersona'
      this.initRequest()
      loginUsuarioApi(credenciales).then((response) => {
        localStorage.setItem('token', response.data.token)
        getPersonaLogueadaApi(credenciales.nombreUsuario).then((response) => {
          this.personaLogueada = response.data
          localStorage.setItem('personaLogueada', JSON.stringify(response.data))
          this.successfulEnding(response, tipoPeticion)
        })
          .catch((error) => {
            this.logoutPersona()
            this.processError(error, tipoPeticion)
          })
      })
        .catch((error) => {
          this.logoutPersona()
          this.processError(error, tipoPeticion)
        })
    },
    logoutPersona() {
      localStorage.removeItem('token')
      localStorage.removeItem('personaLogueada')
      useModelosStore().$reset()
      useEquiposStore().$reset()
      useIncidenciasStore().$reset()
      useUnidadesStore().$reset()
      this.$reset()
    },
    seleccionaPersona(personaLink) {
      let tipoPeticion = 'seleccionaPersona'
      let index = this.listaPersonas.findIndex((c) => c._links.self.href == personaLink)
      this.personaSeleccionada = (index > -1) ? this.listaPersonas[index] : null
      this.initRequest()
      getPersonaApi(personaLink).then((response) => {
        this.actualizaPersonaDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    async compruebaMiembroGC(tip) {
      let tipoPeticion = 'compruebaMiembroGC'
      let miembroGC = null
      let index = this.listaPersonas.findIndex((c) => c.tip == tip)

      if (index > -1) {
        this.personaSeleccionada = this.listaPersonas[index]
      } else {
        this.initRequest()
        await getPersonaPorTipApi(tip).then((response) => {
          miembroGC = response.data
          this.successfulEnding(response, tipoPeticion)
        })
          .catch((error) => this.processError(error, tipoPeticion))
      }
      return miembroGC
    },
    getResolutores() {
      let tipoPeticion = 'getResolutores'
      this.initRequest()
      const tiposDePersonas = Constantes.getTiposDePersonas()
      const tiposDePersonasFormateado = Constantes.getListaDePersonas()
      getResolutoresApi().then((response) => {
        this.listaResolutores = response.data._embedded ? response.data._embedded.personas : []
        this.listaResolutores.forEach(resolutor => {
          const tipoPersona = tiposDePersonasFormateado[tiposDePersonas.indexOf(resolutor.tipoPersona)]
          resolutor.nombreCompleto = `${resolutor.nombre} ${resolutor.apellidos} (${tipoPersona})`
        })
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    getPersonal() {
      let tipoPeticion = 'getPersonal'
      this.initRequest()
      const tiposDePersonas = Constantes.getTiposDePersonas()
      const tiposDePersonasFormateado = Constantes.getListaDePersonas()
      getPersonalApi().then((response) => {
        this.listaPersonal = response.data._embedded ? response.data._embedded.personas : []
        this.successfulEnding(response, tipoPeticion)
       
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    initRequest() {
      this.queriesSinRespuesta++
      this.erroresPersonasStore = false
    },
    successfulEnding(response, tipoPeticion) {
      if (this.queriesSinRespuesta > 0) {
        this.queriesSinRespuesta--
      }
      this.resultadoPeticionPersonas = getMensajeResultadoPeticion(response, tipoPeticion)
    },
    processError(error, tipoPeticion) {
      this.erroresPersonasStore = true
      this.queriesSinRespuesta = this.queriesSinRespuesta > 0 ? --this.queriesSinRespuesta : 0
      this.resultadoPeticionPersonas = getMensajeResultadoPeticion(error, tipoPeticion)
    },
    resetEstadoPersonasStore() {
      this.queriesSinRespuesta = 0
      this.erroresPersonasStore = false
      this.resultadoPeticionPersonas = null
    },
    actualizaPersonaDeLista(persona) {
      let index = this.listaPersonas.findIndex((e) => e._links.self.href == persona._links.self.href)

      if (index > -1) {
        this.listaPersonas[index] = persona
      } else {
        this.listaPersonas.push(persona)
      }
      this.personaSeleccionada = persona
    },
  },
})