![Logo](https://git.institutomilitar.com/Camope/gatel/-/wikis/img/GATEL_TEXTO.png)

# Descripción General

GATEL (Gestión de Activos de Telecomunicaciones) es una aplicación web para la gestión de equipamiento de IT. Consta de un conjunto de funcionalidades que le permiten controlar el inventario de equipos (alta, edición y baja), así como gestionar las incidencias que puedan producirse en los mismos.

El acceso a la aplicación implementa securización que, unido a 4 tipos de roles de usuario, permite limitar tanto las vistas como las funcionalidades y acciones que se pueden ejecutar.

Para el diseño de GATEL se ha seguido una arquitectura de 3 capas, con separación de los niveles de presentación, de aplicación y de datos.

## Estado del Proyecto

Este proyecto se inció el 7 de septiembre de 2023 y se encuentra en **proceso de desarrollo**. Hasta el momento actual (21 de noviembre de 2023) se han realizado dos sprint, tras los cuales se ha completado el desarrollo del **MVP**.

## Requisitos del entorno de desarrollo

- Sistema Operativo Linux o Windows
- Java JDK 17
- Eclipse + Spring Tools 4
- Base de Datos PostgreSQL
- Visual Studio Code
- Node.js 20 o posterior
- Docker Desktop
- Git
- Postman


## Tecnologías y entornos de desarrollo/despliegue

- **Frontend**: Está implementado en HTML5, CSS y JavaScript a través del framework Vue.js, y alojado en Netlify.

- **Backend**: API desarrollada en lenguaje Java con el framework Spring Boot y alojada en una instancia VPS de AWS (Lightsail).

- **Datos**: BBDD PostgreSQL alojada en ElephantSQL.

## Funcionalidades del proyecto

- Alta, edición y borrado de Modelos (plantillas base para un Equipo).
- Alta y borrado de Equipos.
- Gestión de Incidencias.
- Datos estadísticos de Equipos e Incidencias.
- Buscador y ordenación en los listados.
- Vistas personalizas por usuario y rol.

## Equipo del Proyecto
- **Scrum Master**: Capitán Juan Jesús Díaz Gómez.
- **Product Owner**: Teniente. Alejandro González Escobar.
- **Developers**:
    - Comandante Carlos Andrés Moreno Pérez (@camope)
    - Capitán Tomás Carrasco del Rey (@samotcarrasco). 

## Diagrama de clases

![Diagrama](https://git.institutomilitar.com/Camope/gatel/-/wikis/img/Diagrama_clases_GATEL.png)


## URLs del proyecto:
   
![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white) Repositorio proyecto: https://git.institutomilitar.com/Camope/gatel

![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white) Repositorio librería: https://github.com/samotcarrasco/gatel-lib
 
 ![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white) Despliegue API: https://gatel-api.vp7o6vfdm7heu.eu-west-3.cs.amazonlightsail.com/api 

 ![Postman](https://img.shields.io/badge/Postman-FF6C37?style=for-the-badge&logo=postman&logoColor=white) Documentación API: https://documenter.getpostman.com/view/24984973/2s9YeAAuPG
 
 [![Netlify Status](https://api.netlify.com/api/v1/badges/319f274a-2f89-422c-bc12-4865e70526fb/deploy-status)](https://app.netlify.com/sites/gatel/deploys)
 Despliegue de la API en Internet: https://gatel.netlify.com 



