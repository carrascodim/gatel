const FILTRADO = true
const FILTROS = {
  'opcionesMenuPrincipal': {
    'equipos': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO'],
    'incidencias': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO', 'RESOLUTOR'],
    'estadisticas': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'personalUnidades': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'estadisticas': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'ayuda': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO', 'RESOLUTOR'],
  },
  'opcionesMenuEquipos': {
    'MODELOS': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'INVENTARIO': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO'],
  },
  'opcionesMenuPersonalUnidades': {
    'PERSONAL': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'UNIDADES': ['ADMIN_CENTRAL'],
  },
  'accionesModelos': {
    'editar': ['ADMIN_CENTRAL'],
    'informar': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'borrar': ['ADMIN_CENTRAL'],
    'agregar': ['ADMIN_CENTRAL'],
    'nuevo' : ['ADMIN_CENTRAL']
  },
  'accionesEquipos': {
    'editar': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'informar': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO'],
    'borrar': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'reportarIncidencia': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO'],
  },
  'accionesIncidencias': {
    'editar': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'RESOLUTOR'],
    'informar': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO', 'RESOLUTOR'],
  },
  'rutas': {
    'home': [],
    'login': [],
    'equipos': [],
    'modelos': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'inventario': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO'],
    'personalUnidades': [],
    'personal': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD'],
    'unidades': ['ADMIN_CENTRAL'],
    'about': [],
    'ayuda': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO', 'RESOLUTOR'],
    'incidencias': ['ADMIN_CENTRAL', 'ADMIN_UNIDAD', 'USUARIO', 'RESOLUTOR'],
    'estadisticas': ['ADMIN_CENTRAL', , 'ADMIN_UNIDAD'],
  }
}

export function estaAutorizado(grupoDeOpciones, opcion, rol) {
  let resultado = false
  if (FILTRADO) {
    if (FILTROS.hasOwnProperty(grupoDeOpciones)) {
      if (FILTROS[grupoDeOpciones].hasOwnProperty(opcion)) {
        resultado = FILTROS[grupoDeOpciones][opcion].includes(rol)
      }
    }
  } else {
    resultado = true
  }

  return resultado
}
