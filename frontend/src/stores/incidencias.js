import { defineStore } from 'pinia'
import { getMensajeResultadoPeticion } from '@/js/errors'
import {
  getIncidenciasApi, postIncidenciaApi, patchIncidenciaApi, getIncidenciaApi,
  patchAsignarResolutorApi, patchResolverIncidenciaApi, patchCerrarIncidenciaApi
} from '@/stores/api-service'
import { stringToDate } from '@/js/utils'


export const useIncidenciasStore = defineStore('incidencia', {
  state: () => ({
    queriesSinRespuesta: 0,
    erroresIncidenciasStore: false,
    listaIncidencias: [],
    incidenciaSeleccionada: null,
    resultadoPeticionIncidencias: null,

  }),
  getters: {
    loadingIncidenciasStore() {
      return this.queriesSinRespuesta > 0
    }
  },

  actions: {
    getIncidencias() {
      let tipoPeticion = 'getIncidencias'
      this.initRequest()
      getIncidenciasApi().then((response) => {
        this.listaIncidencias = response.data._embedded ? response.data._embedded.incidencias : []
        this.listaIncidencias.forEach(i => this.formateaIncidencia(i))
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    saveIncidencia(incidencia) {
      let tipoPeticion = 'saveIncidencia'
      this.initRequest()

      postIncidenciaApi(incidencia).then((response) => {
        this.actualizaIncidenciaDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    seleccionaIncidencia(incidenciaLink) {
      let tipoPeticion = 'seleccionaIncidencia'
      let index = this.listaIncidencias.findIndex((c) => c._links.self.href == incidenciaLink)
      this.incidenciaSeleccionada = (index > -1) ? this.listaIncidencias[index] : null
      this.initRequest()
      getIncidenciaApi(incidenciaLink).then((response) => {
        this.actualizaIncidenciaDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },

    editIncidencia(incidencia) {
      this.initRequest()
      let tipoPeticion = 'editIncidencia'
      let incidenciaBody = { ...incidencia }
      incidenciaBody.detalles = incidencia.nuevoDetalle
      patchIncidenciaApi(incidencia._links.self.href, incidenciaBody).then((response) => {
        this.actualizaIncidenciaDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },

    patchAsignarResolutor(incidencia, resolutor) {
      const body = {
        agenteResolutor: resolutor._links.self.href
      }
      this.initRequest()
      let tipoPeticion = 'patchAsignarResolutor'

      return patchAsignarResolutorApi(incidencia.id, body).then((response) => {
        this.actualizaIncidenciaDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
        return response
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },


    async patchResolverIncidencia(incidencia) {
      this.initRequest()
      let tipoPeticion = 'patchResolverIncidencia'
      let incidenciaBody = { ...incidencia }
      incidenciaBody.detalles = incidencia.nuevoDetalle
      await patchIncidenciaApi(incidencia._links.self.href, incidenciaBody).then((response) => {
        patchResolverIncidenciaApi(incidencia.id).then((response) => {
          this.actualizaIncidenciaDeLista(response.data)
          this.successfulEnding(response, tipoPeticion)
        }).catch((error) => this.processError(error, tipoPeticion))
      })
        .catch((error) => this.processError(error, tipoPeticion))

     },

    patchCerrarIncidencia(incidencia) {
      this.initRequest()
      let tipoPeticion = 'patchCerrarIncidencia'
      return patchCerrarIncidenciaApi(incidencia.id).then((response) => {
        this.actualizaIncidenciaDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
        return response
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },

    initRequest() {
      this.queriesSinRespuesta++
      this.erroresIncidenciasStore = false
    },
    successfulEnding(response, tipoPeticion) {
      if (this.queriesSinRespuesta > 0) {
        this.queriesSinRespuesta--
      }
      this.resultadoPeticionIncidencias = getMensajeResultadoPeticion(response, tipoPeticion)

    },
    processError(error, tipoPeticion) {
      this.erroresEquiposStore = true
      this.queriesSinRespuesta = this.queriesSinRespuesta > 0 ? --this.queriesSinRespuesta : 0
      this.resultadoPeticionEquipos = getMensajeResultadoPeticion(error, tipoPeticion)

    },
    resetEstadoIncidenciasStore() {
      this.queriesSinRespuesta = 0
      this.erroresIncidenciasStore = false
      this.resultadoPeticionIncidencias = null

    },
    resetResultadoPeticionIncidencias() {
      this.resultadoPeticionIncidencias = null
    },
    actualizaIncidenciaDeLista(incidencia) {
      this.formateaIncidencia(incidencia)
      let index = this.listaIncidencias.findIndex((e) => e._links.self.href == incidencia._links.self.href)

      if (index > -1) {
        this.listaIncidencias[index] = incidencia
      } else {
        this.listaIncidencias.push(incidencia)
      }
      this.incidenciaSeleccionada = incidencia
    },
    formateaIncidencia(incidencia) {
      incidencia.fechaAlta = stringToDate(incidencia.fechaAlta)
      incidencia.fechaAsignacion = stringToDate(incidencia.fechaAsignacion)
      incidencia.fechaCierre = stringToDate(incidencia.fechaCierre)
      incidencia.fechaResolucion = stringToDate(incidencia.fechaResolucion)
    },
  },
})