package es.mdef.apigatel.REST;

import java.time.LocalDate;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import es.mde.acing.gatel.IncidenciaImpl.EstadoIncidencia;
import es.mde.acing.gatel.IncidenciaImpl.TipoIncidencia;

@Relation(itemRelation = "incidencia")
public class IncidenciaModel extends RepresentationModel<IncidenciaModel> {

	private Long id;
	private String codigo;
	private LocalDate fechaAlta;
	private LocalDate fechaResolucion;
	private EstadoIncidencia estado;
	private String descripcion;
	private String equipoN;
	private String detalles;
	private LocalDate fechaCierre;
	private LocalDate fechaAsignacion;
	private TipoIncidencia tipoIncidencia;
	private String resolutorN;
	private String resolutorEmail;
	private String informador;

	// Averia
	private String componente;
	private Boolean reparable;

	// Extravio
	private String ultimaUbicacion;
	private boolean bloqueado;
	private boolean borrado;
	private boolean encontrado;

	// Configuracion
	private String aplicacion;

	// Solicitud
	private Boolean aceptado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public LocalDate getFechaResolucion() {
		return fechaResolucion;
	}

	public void setFechaResolucion(LocalDate fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

	public EstadoIncidencia getEstado() {
		return estado;
	}

	public void setEstado(EstadoIncidencia estado) {
		this.estado = estado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public TipoIncidencia getTipoIncidencia() {
		return tipoIncidencia;
	}

	public void setTipoIncidencia(TipoIncidencia tipoIncidencia) {
		this.tipoIncidencia = tipoIncidencia;
	}

	public String getComponente() {
		return componente;
	}

	public void setComponente(String componente) {
		this.componente = componente;
	}

	public Boolean getReparable() {
		return reparable;
	}

	public void setReparable(Boolean reparable) {
		this.reparable = reparable;
	}

	public String getUltimaUbicacion() {
		return ultimaUbicacion;
	}

	public void setUltimaUbicacion(String ultimaUbicacion) {
		this.ultimaUbicacion = ultimaUbicacion;
	}

	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public boolean isBorrado() {
		return borrado;
	}

	public void setBorrado(boolean borrado) {
		this.borrado = borrado;
	}

	public boolean isEncontrado() {
		return encontrado;
	}

	public void setEncontrado(boolean encontrado) {
		this.encontrado = encontrado;
	}

	public String getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Boolean isAceptado() {
		return aceptado;
	}

	public void setAceptado(Boolean aceptado) {
		this.aceptado = aceptado;
	}

	public String getEquipoN() {
		return equipoN;
	}

	public void setEquipoN(String equipoN) {
		this.equipoN = equipoN;
	}

	public String getDetalles() {
		return detalles;
	}

	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}

	public LocalDate getFechaCierre() {
		return fechaCierre;
	}

	public void setFechaCierre(LocalDate fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public LocalDate getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(LocalDate fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public String getResolutorN() {
		return resolutorN;
	}

	public void setResolutorN(String resolutorN) {
		this.resolutorN = resolutorN;
	}

	public String getResolutorEmail() {
		return resolutorEmail;
	}

	public void setResolutorEmail(String resolutorEmail) {
		this.resolutorEmail = resolutorEmail;
	}

	public String getInformador() {
		return informador;
	}

	public void setInformador(String informador) {
		this.informador = informador;
	}
	
	@Override
	public String toString() {
		return "IncidenciaModel [id=" + id + ", codigo=" + codigo + ", fechaAlta=" + fechaAlta + ", fechaResolucion="
				+ fechaResolucion + ", estado=" + estado + ", descripcion=" + descripcion + ", equipoN=" + equipoN
				+ ", detalles=" + detalles + ", fechaCierre=" + fechaCierre + ", fechaAsignacion=" + fechaAsignacion
				+ ", tipoIncidencia=" + tipoIncidencia + ", resolutorN=" + resolutorN + ", resolutorEmail="
				+ resolutorEmail + ", informador=" + informador + ", componente=" + componente + ", reparable="
				+ reparable + ", ultimaUbicacion=" + ultimaUbicacion + ", bloqueado=" + bloqueado + ", borrado="
				+ borrado + ", encontrado=" + encontrado + ", aplicacion=" + aplicacion + ", aceptado=" + aceptado
				+ "]";
	}
	

}
