import { defineStore } from 'pinia'
import { getMensajeResultadoPeticion } from '@/js/errors'
import { getUnidadesApi, getUnidadApi } from '@/stores/api-service'

export const useUnidadesStore = defineStore('unidad', {
  state: () => ({
    queriesSinRespuesta: 0,
    erroresUnidadesStore: false,
    listaUnidades: [],
    unidadSeleccionada: null,
    resultadoPeticionUnidades: null,
  }),
  getters: {
    loadingUnidadesStore() {
      return this.queriesSinRespuesta > 0
    }
  },
  actions: {
    getUnidades() {
      this.initRequest()
      getUnidadesApi().then((response) => {
        this.listaUnidades = response.data._embedded ? response.data._embedded.unidades : []
        this.successfulEnding(response, 'getUnidades')
      })
        .catch((error) => this.processError(error, 'getUnidades'))
    },
    seleccionaUnidad(unidadLink) {
      let index = this.listaUnidades.findIndex((c) => c._links.self.href == unidadLink)
      this.unidadSeleccionada = (index > -1) ? this.listaUnidades[index] : null
      this.initRequest()
      getUnidadApi(unidadLink).then((response) => {
        this.actualizaUnidadDeLista(response.data)
        this.successfulEnding(response, 'seleccionaUnidad')
      })
        .catch((error) => this.processError(error, 'seleccionaUnidad'))
    },

    initRequest() {
      this.queriesSinRespuesta++
      this.erroresUnidadesStore = false
    },
    successfulEnding(response, tipoPeticion) {
      if (this.queriesSinRespuesta > 0) {
        this.queriesSinRespuesta--
      }
      this.resultadoPeticionUnidades = getMensajeResultadoPeticion(response, tipoPeticion)
    },
    processError(error, tipoPeticion) {
      this.erroresUnidadesStore = true
      this.queriesSinRespuesta = this.queriesSinRespuesta > 0 ? --this.queriesSinRespuesta : 0
      this.resultadoPeticionUnidades = getMensajeResultadoPeticion(error, tipoPeticion)
    },
    resetEstadoUnidadesStore() {
      this.queriesSinRespuesta = 0
      this.erroresUnidadesStore = false
      this.resultadoPeticionUnidades = null
    },
    resetPeticionesUnidadesStore() {
      this.queriesSinRespuesta = 0
    },
    resetErroresUnidadesStore() {
      this.erroresModelosStore = false
    },
    resetResultadoPeticionUnidades() {
      this.resultadoPeticionUnidades = null
    },
    actualizaUnidadDeLista(unidad) {
      let index = this.listaUnidades.findIndex((e) => e._links.self.href == unidad._links.self.href)

      if (index > -1) {
        this.listaUnidades[index] = unidad
      } else {
        this.listaUnidades.push(unidad)
      }
      this.unidadSeleccionada = unidad
    },
  },
})