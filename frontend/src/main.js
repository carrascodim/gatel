import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import { createPinia } from 'pinia'

import App from '@/App.vue'
const MenuEquipos = () => import('@/components/MenuEquipos.vue')
const Modelos = () => import('@/components/Modelos.vue')
const Inventario = () => import('@/components/Inventario.vue')
const Incidencias = () => import('@/components/Incidencias.vue')
const Estadisticas = () => import('@/components/Estadisticas.vue')
const About = () => import('@/components/About.vue')
const NotFound = () => import('@/components/NotFound.vue')
const FormularioLogin = () => import('@/components/FormularioLogin.vue')
const MenuPersonalUnidades = () => import('@/components/MenuPersonalUnidades.vue')
const Personal = () => import('@/components/Personal.vue')
const Unidades = () => import('@/components/Unidades.vue')
const Ayuda = () => import('@/components/Ayuda.vue')

import PrimeVue from 'primevue/config'
import Button from 'primevue/button'
import TabMenu from 'primevue/tabmenu'
import Paginator from 'primevue/paginator'
import Dropdown from 'primevue/dropdown'
import Tag from 'primevue/tag'
import ProgressSpinner from 'primevue/progressspinner'
import Dialog from 'primevue/dialog'
import InputText from 'primevue/inputtext'
import Textarea from 'primevue/textarea'
import FileUpload from 'primevue/fileupload'
import RadioButton from 'primevue/radiobutton'
import InputSwitch from 'primevue/inputswitch'
import InputNumber from 'primevue/inputnumber'
import Calendar from 'primevue/calendar'
import ToastService from 'primevue/toastservice'
import Toast from 'primevue/toast'
import Timeline from 'primevue/timeline'
import Tooltip from 'primevue/tooltip'


import '@/scss/styles.scss'
import * as bootstrap from '~bootstrap'
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'bootstrap/dist/css/bootstrap-grid.min.css'

// import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/themes/lara-light-teal/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'



const pinia = createPinia()

const routes = [
  { path: '/', name: 'home', component: About },
  { path: '/login', name: 'login', component: FormularioLogin },
  {
    path: '/equipos', name: 'equipos', component: MenuEquipos, children: [
      { path: 'modelos', name: 'modelos', component: Modelos },
      { path: 'inventario', name: 'inventario', component: Inventario },
    ]
  },
  {
    path: '/personalUnidades', name: 'personalUnidades', component: MenuPersonalUnidades, children: [
      { path: 'personal', name: 'personal', component: Personal },
      { path: 'unidades', name: 'unidades', component: Unidades }
    ]
  },
  { path: '/about', name: 'about', component: About },
  { path: '/incidencias', name: 'incidencias', component: Incidencias },
  { path: '/estadisticas', name: 'estadisticas', component: Estadisticas },
  { path: '/ayuda', name: 'ayuda', component: Ayuda },
  { path: '/:pathMatch(.*)*', name: 'notFound', component: NotFound },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})


import { usePersonasStore } from '@/stores/personas'
import { estaAutorizado } from '@/js/filtros'
const rutasPorDefecto = {
  'ADMIN_CENTRAL': 'modelos',
  'ADMIN_UNIDAD': 'modelos',
  'USUARIO': 'inventario',
  'RESOLUTOR': 'incidencias'
}
router.beforeEach(async (to, from) => {
  let personaLogueada = usePersonasStore().personaLogueada
  let ruta = null

  if (personaLogueada) {
    let perfil = usePersonasStore().personaLogueada.perfil
    let name = to.name

    if (name == 'equipos') {
      if ((perfil == 'ADMIN_CENTRAL') || (perfil == 'ADMIN_UNIDAD')) {
        name = 'modelos'
      } else if (perfil == 'USUARIO') {
        name = 'inventario'
      }
    } else if (name == 'personalUnidades') {
      name = 'personal'
    }

    if (!estaAutorizado('rutas', name, perfil)) {
      name = rutasPorDefecto[perfil]
    }

    if (name != to.name) {
      ruta = { name }
    }

  } else {
    if (to.name != 'login') {
      ruta = { name: 'login' }
    }
  }

  return ruta
})


const app = createApp(App)

app.use(pinia)
app.use(router)
app.use(PrimeVue, {
  locale: {
    firstDayOfWeek: 1,
    dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
  }
})
app.use(ToastService)

app.directive('tooltip', Tooltip)

app.component('Button', Button)
app.component('TabMenu', TabMenu)
app.component('Paginator', Paginator)
app.component('Dropdown', Dropdown)
app.component('Tag', Tag)
app.component('Dialog', Dialog)
app.component('InputText', InputText)
app.component('Textarea', Textarea)
app.component('FileUpload', FileUpload)
app.component('RadioButton', RadioButton)
app.component('InputSwitch', InputSwitch)
app.component('InputNumber', InputNumber)
app.component('Calendar', Calendar)
app.component('ProgressSpinner', ProgressSpinner)
app.component('Toast', Toast)
app.component('Timeline', Timeline)

app.mount('#app')