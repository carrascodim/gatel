export const Constantes = {
  getTiposDeEquipo(){
    return [
      'EQUIPO_INFORMATICO',
      'WEBCAM',
      'AURICULARES'
    ]
  },
  getListaDeEquipos(){
    return [
      'Equipo Informático',
      'Webcam',
      'Auriculares'
    ]
  },
  getTipoEquipoInformatico() {
    return [
      'Ordenador portátil',
      'Tablet',
      'Smartphone'
    ]
  },
  getTiposDeIncidencia(){
    return [
      'AVERIA',
      'CONFIGURACION',
      'EXTRAVIO',
      'SOLICITUD'
    ]
  }, 
  getListaDeIncidencias(){
    return [
      'Avería',
      'Configuración',
      'Extravío',
      'Solicitud'
    ]
  },
  getEstadosDeIncidencia(){
    return [
    	'NUEVA',
		  'ASIGNADA',
		  'RESUELTA',
		  'CERRADA'
    ]
  }, 
   getRoles() {
    return [
      'ADMIN',
      'NO_ADMIN'
    ]
  },
  getPerfiles() {
    return [
      'ADMIN_CENTRAL',
      'ADMIN_UNIDAD',
      'USUARIO',
      'RESOLUTOR'
    ]
  },
  getTiposDePersonas(){
    return [
      'MIEMBRO_GC',
      'PERSONAL_EXTERNO'
    ]
  },
  getListaDePersonas(){
    return [
      'Miembro GC',
      'Externo'
        ]
  },
  getResoluciones() {
    return [
      "SD (480p)", 
      "1K (540p)", 
      "HD (720p)", 
      "Full HD (1080p)",
      "4K (2160p)",
      "8K (4320p)"
    ]
  },
  getConexiones() {
    return [
      "Bluetooth",
      "Jack 3.5mm",
      "Jack 6.35mm",
      "USB tipo A",
      "USB tipo C"
    ]
  },
 
  colores() {
    return [
      'rgba(255, 159, 64, 0.4)',   // Naranja oscuro
      'rgba(75, 192, 192, 0.4)',   // Verde oscuro
      'rgba(54, 162, 235, 0.4)',   // Azul oscuro
      'rgba(255, 99, 132, 0.4)',   // Rosa oscuro
      'rgba(255, 205, 86, 0.4)',   // Amarillo oscuro
      'rgba(153, 102, 255, 0.4)',  // Lila oscuro
      'rgb(190, 242, 176)',  // Verde pastel
      'rgba(173, 216, 230, 0.8)',  // Azul celeste oscuro 
    ]
  },
  
  coloresBorde() {
    return [
      'rgb(255, 159, 64)',    // Naranja oscuro
      'rgb(75, 192, 192)',    // Verde oscuro
      'rgb(54, 162, 235)',    // Azul oscuro
      'rgb(255, 99, 132)',    // Rosa oscuro
      'rgb(255, 205, 86)',    // Amarillo oscuro
      'rgb(153, 102, 255)',   // Lila oscuro
      'rgb(141, 194, 155)',   //verde
      'rgb(85, 137, 158)',    // Azul celeste oscuro 
    ]
  },

  getConstantes() {
    return {
      PULGADAS_MAX: 21,
      PULGADAS_MIN: 4,
      PULGADAS_DEF: 5,
      PULGADAS_PORTATIL_DEF: 15,
      PULGADAS_TABLET_DEF: 10,
      PULGADAS_SMARTPHONE_DEF: 5,
      DISCODURO_MAX: 8192,
      DISCODURO_MIN: 32,
      MEMORIA_MAX: 128,
      MEMORIA_MIN: 1,
      IMAGEN_REDUCIDA_ANCHO: 80,
      IMAGEN_REDUCIDA_ALTO: 80,
    }
  },
}

export const TOAST_LIFE_TIME = 5000