package es.mdef.apigatel.REST;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

//import es.mde.acing.gatel.Equipo;
import es.mde.acing.gatel.ModeloImpl.TipoModelo;

@Relation(collectionRelation = "modelos")
public class ModeloListaModel extends RepresentationModel<ModeloListaModel> {

	private Long id;
	private String marca;
	private String nombreModelo;
	private String imgReducida;
	private Integer stock;
	private TipoModelo tipoModelo;
	private String tipoEquipoInformatico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImgReducida() {
		return imgReducida;
	}

	public void setImgReducida(String imgReducida) {
		this.imgReducida = imgReducida;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getNombreModelo() {
		return nombreModelo;
	}

	public void setNombreModelo(String nombreModelo) {
		this.nombreModelo = nombreModelo;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public TipoModelo getTipoModelo() {
		return tipoModelo;
	}

	public void setTipoModelo(TipoModelo tipoModelo) {
		this.tipoModelo = tipoModelo;
	}

	
	public String getTipoEquipoInformatico() {
		return tipoEquipoInformatico;
	}

	public void setTipoEquipoInformatico(String tipoEquipoInformatico) {
		this.tipoEquipoInformatico = tipoEquipoInformatico;
	}
	
	@Override
	public String toString() {
		return "ModeloListaModel [id=" + id + ", marca=" + marca + ", nombreModelo=" + nombreModelo // + ", equipos=" +
																									// equipos
				+ ", imgReducida=" + imgReducida + ", tipoModelo=" + tipoModelo + "]";
	}


}
