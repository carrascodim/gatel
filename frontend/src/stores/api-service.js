import axios from 'axios'

// const host = 'http://localhost:8080/api'
//const host = 'https://gatel-api.vp7o6vfdm7heu.eu-west-3.cs.amazonlightsail.com/api'
const host = 'https://apigatel-1i5qhf75.b4a.run/api'
const HTTP_TO_HTTPS_ENABLE = true

axios.interceptors.response.use(function (response) {
  let token = response.headers.getAuthorization()
  if(token) {
    localStorage.setItem('token', token)
  }
  return response
}, function (error) {
  return Promise.reject(error)
})

axios.interceptors.request.use(function (config) {
  let token = localStorage.getItem('token')
  if(token) {
    config.headers.setAuthorization('Bearer ' + token)  
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

function apiRequest(path, method, body) {
  let config = {
    method: method ?? 'get',
    maxBodyLength: Infinity,
    url: path,
    timeout: 30000,
    headers: {}
  }

  config.url = httpToHttps(path)

  if (body) {
    if (body instanceof FormData) {
      config.headers = { 'Content-Type': 'multipart/form-data' }
    } else {
      config.headers = { 'Content-Type': 'application/json' }
    }
    config.data = body
  }

  return axios.request(config)
}

function getApi(url) {
  return apiRequest(url, 'get')
}

function postApi(url, body) {
  return apiRequest(url, 'post', body)
}

function putApi(url, body) {
  return apiRequest(url, 'put', body)
}

function deleteApi(url) {
  return apiRequest(url, 'delete')
}

function patchApi(url, body) {
  return apiRequest(url, 'patch', body)
}

export async function getModelosApi() {
  return getApi(`${host}/modelos`)
}

export async function getModeloApi(modeloLink) {
  return getApi(modeloLink)
}

export async function postModeloApi(modelo) {
  return postApi(`${host}/modelos`, modelo)
}

export async function deleteModeloApi(modeloLink) {
  return deleteApi(modeloLink)
}

export async function putModeloApi(modeloLink, body) {
  return putApi(modeloLink, body)
}

export async function patchAsignarResolutorApi(incidenciaId, body) {
  return patchApi(`${host}/incidencias/asignarIncidencia/${incidenciaId}`, body)
}

export async function patchResolverIncidenciaApi(incidenciaId) {
  return patchApi(`${host}/incidencias/resolverIncidencia/${incidenciaId}`)
}

export async function patchCerrarIncidenciaApi(incidenciaId) {
  return patchApi(`${host}/incidencias/cerrarIncidencia/${incidenciaId}`)
}

export async function getIncidenciasApi() {
  return getApi(`${host}/incidencias`)
}

export async function getIncidenciaApi(incidenciaLink) {
  return getApi(incidenciaLink)
}

export async function postIncidenciaApi(incidencia) {
  return postApi(`${host}/incidencias`, incidencia)
}

export async function deleteIncidenciaApi(incidenciaLink) {
  return deleteApi(incidenciaLink)
}

export async function putIncidenciaApi(incidenciaLink, body) {
  return putApi(incidenciaLink, body)
}

export async function patchIncidenciaApi(incidenciaLink, body) {
  return patchApi(incidenciaLink, body)
}

export async function getEquiposApi() {
  return getApi(`${host}/equipos`)
}

export async function getEquipoApi(equipoLink) {
  return getApi(equipoLink)
}

export async function postEquipoApi(equipo) {
  return postApi(`${host}/equipos`, equipo)
}

export async function deleteEquipoApi(equipoLink) {
  return deleteApi(equipoLink)
}

export async function putEquipoApi(equipoLink, body) {
  return putApi(equipoLink, body)
}

export async function asignarEquipoApi(body) {
  return patchApi(`${host}/equipos/asignarEquipo`, body)
}

export async function desvincularEquipoApi(id) {
  return patchApi(`${host}/equipos/desasignarEquipo/${id}`)
}

export async function loginUsuarioApi(credenciales) {
  let form = new FormData()
  form.append('nombreUsuario', credenciales.nombreUsuario)
  form.append('password', credenciales.password)
  return postApi(`${host}/login`, form)
}

export async function getPersonaLogueadaApi() {
  return getApi(`${host}/usuario`)
}

export async function getPersonaApi(personaLink) {
  return getApi(personaLink)
}

export async function getPersonaPorTipApi(tip) {
  return getApi(`${host}/personas/tip/${tip}`)
}

export async function getUnidadesApi() {
  return getApi(`${host}/unidades`)
}

export async function getUnidadApi(unidadLink) {
  return getApi(unidadLink)
}

export async function getResolutoresApi() {
  return getApi(`${host}/personas/resolutores`)
}

export async function getPersonalApi() {
  return getApi(`${host}/personas/personal`)
}
function httpToHttps(link) {

  if (HTTP_TO_HTTPS_ENABLE && link.startsWith('http:')) {
    link = link.replace('http:', 'https:')
  }

  return link
}
