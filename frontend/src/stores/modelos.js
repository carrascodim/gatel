import { defineStore } from 'pinia'
import { getMensajeResultadoPeticion } from '@/js/errors'
import {
  getModelosApi, postModeloApi, putModeloApi, deleteModeloApi,
  getModeloApi
} from '@/stores/api-service'

export const useModelosStore = defineStore('modelo', {
  state: () => ({
    queriesSinRespuesta: 0,
    erroresModelosStore: false,
    listaModelos: [],
    modeloSeleccionado: null,
    resultadoPeticionModelos: null,
  }),
  getters: {
    loadingModelosStore() {
      return this.queriesSinRespuesta > 0
    },
  },

  actions: {
    getModelos() {
      let tipoPeticion = 'getModelos'
      this.initRequest()
      getModelosApi().then((response) => {
        this.listaModelos = response.data._embedded ? response.data._embedded.modelos : []
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    saveModelo(modelo) {
      let tipoPeticion = 'saveModelo'
      let body = this.modeloToBody(modelo)
      this.initRequest()
      postModeloApi(body).then((response) => {
        this.actualizaModeloDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) =>
          this.processError(error, tipoPeticion))
    },
    seleccionaModelo(modeloLink) {
      let tipoPeticion = 'seleccionaModelo'
      let index = this.listaModelos.findIndex((c) => c._links.self.href == modeloLink)
      this.modeloSeleccionado = (index > -1) ? this.listaModelos[index] : null
      this.initRequest()
      getModeloApi(modeloLink).then((response) => {
        this.actualizaModeloDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
        })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    editModelo(modelo) {
      let tipoPeticion = 'editModelo'
      let body = this.modeloToBody(modelo)
      this.initRequest()
      putModeloApi(modelo._links.self.href, body).then((response) => {
        this.actualizaModeloDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    removeModelo(modelo) {
      let tipoPeticion = 'removeModelo'
      this.initRequest()
      deleteModeloApi(modelo._links.self.href).then((response) => {
        let indexToRemove = this.listaModelos.indexOf(modelo)
        if (indexToRemove > -1) this.listaModelos.splice(indexToRemove, 1)
        this.modeloSeleccionado = null
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    initRequest() {
      this.queriesSinRespuesta++
      this.erroresModelosStore = false
    },
    successfulEnding(response, tipoPeticion) {
      if (this.queriesSinRespuesta > 0) {
        this.queriesSinRespuesta--
      }
      this.resultadoPeticionModelos = getMensajeResultadoPeticion(response, tipoPeticion)
    },
    processError(error, tipoPeticion) {
      this.erroresModelosStore = true
      this.queriesSinRespuesta = this.queriesSinRespuesta > 0 ? --this.queriesSinRespuesta : 0
      this.resultadoPeticionModelos = getMensajeResultadoPeticion(error, tipoPeticion)
    },
    resetEstadoModelosStore() {
      this.queriesSinRespuesta = 0
      this.erroresModelosStore = false
      this.resultadoPeticionModelos = null
    },
    resetPeticionesModelosStore() {
      this.queriesSinRespuesta = 0
    },
    resetErroresModelosStore() {
      this.erroresModelosStore = false
    },
    resetResultadoPeticionModelos() {
      this.resultadoPeticionModelos = null
    },
    modeloToBody({ marca, nombreModelo, detalles, stereo, conexion, resolucion, pulgadas, discoDuro, memoria, sistemaOperativo, tipoModelo, tipoEquipoInformatico, imagen, imgReducida }) {
      return { marca, nombreModelo, detalles, stereo, conexion, resolucion, pulgadas, discoDuro, memoria, sistemaOperativo, tipoModelo, tipoEquipoInformatico, imagen, imgReducida }
    },
    addStockItem(link) {
      let index = this.listaModelos.findIndex((u) => u._links.self.href == link)
      this.listaModelos[index].stock++
    },
    actualizaModeloDeLista(modelo) {
      let index = this.listaModelos.findIndex((e) => e._links.self.href == modelo._links.self.href)

      if (index > -1) {
        this.listaModelos[index] = modelo  
      } else {
        this.listaModelos.push(modelo)
      }
      this.modeloSeleccionado = modelo
    },
  },
})