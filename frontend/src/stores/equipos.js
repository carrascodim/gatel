import { defineStore } from 'pinia'
import { getMensajeResultadoPeticion } from '@/js/errors'
import {
  getEquiposApi, postEquipoApi, putEquipoApi, deleteEquipoApi,
  getEquipoApi, asignarEquipoApi, desvincularEquipoApi
} from '@/stores/api-service'
import { addEstadoAsignacion, stringToDate } from '@/js/utils'
import { useModelosStore } from '@/stores/modelos'

export const useEquiposStore = defineStore('equipo', {
  state: () => ({
    queriesSinRespuesta: 0,
    erroresEquiposStore: false,
    listaEquipos: [],
    equipoSeleccionado: null,
    resultadoPeticionEquipos: null,
    palabraBuscadaStore: null,
  }),
  getters: {
    loadingEquiposStore() {
      return this.queriesSinRespuesta > 0
    }
  },
  actions: {
    getEquipos() {
      let tipoPeticion = 'getEquipos'
      this.initRequest()
      getEquiposApi().then((response) => {
        this.listaEquipos = response.data._embedded ? response.data._embedded.equipos : []
        this.listaEquipos.forEach(e => this.formateaEquipo(e))
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },

    saveEquipo(equipo) {
      const modeloStore = useModelosStore()
      let tipoPeticion = 'saveEquipo'
      this.initRequest()

      let body = this.equipoToBody(equipo)

      postEquipoApi(body).then((response) => {
        this.actualizaEquipoDeLista(response.data)
        modeloStore.addStockItem(equipo.modelo)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) =>
          this.processError(error, tipoPeticion))
    },
    seleccionaEquipo(equipoLink) {
      let tipoPeticion = 'seleccionaEquipo'
      let index = this.listaEquipos.findIndex((c) => c._links.self.href == equipoLink)
      this.equipoSeleccionado = (index > -1) ? this.listaEquipos[index] : null
      this.initRequest()
      getEquipoApi(equipoLink).then((response) => {
        this.actualizaEquipoDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    editEquipo(equipo) {
      let tipoPeticion = 'editEquipo'
      let body = this.equipoToBody(equipo)
      this.initRequest()
      putEquipoApi(equipo._links.self.href, body).then((response) => {
        this.actualizaEquipoDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    removeEquipo(equipo) {
      let tipoPeticion = 'removeEquipo'
      this.initRequest()
      deleteEquipoApi(equipo._links.self.href).then((response) => {
        let indexToRemove = this.listaEquipos.indexOf(equipo)
        if (indexToRemove > -1) this.listaEquipos.splice(indexToRemove, 1)
        this.equipoSeleccionado = null
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    asignarEquipoStore(equipo, propietario) {
      let tipoPeticion = 'asignarEquipo'
      let body = {}
      this.initRequest()
      if (equipo.tipoEquipo == 'EQUIPO_PERSONAL') {
        body.persona = propietario._links.self.href
      } else if (equipo.tipoEquipo == 'EQUIPO_UNIDAD') {
        body.unidad = propietario._links.self.href
      }

      body.equipo = equipo._links.self.href

      asignarEquipoApi(body).then((response) => {
        this.actualizaEquipoDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))

    },
    desvincularEquipoStore(equipo) {
      let tipoPeticion = 'desvincularEquipo'
      this.initRequest()
      desvincularEquipoApi(equipo.id).then((response) => {
        this.actualizaEquipoDeLista(response.data)
        this.successfulEnding(response, tipoPeticion)
      })
        .catch((error) => this.processError(error, tipoPeticion))
    },
    initRequest() {
      this.queriesSinRespuesta++
      this.erroresEquiposStore = false
    },
    successfulEnding(response, tipoPeticion) {
      if (this.queriesSinRespuesta > 0) {
        this.queriesSinRespuesta--
      }
      this.resultadoPeticionEquipos = getMensajeResultadoPeticion(response, tipoPeticion)
    },
    processError(error, tipoPeticion) {
      this.erroresEquiposStore = true
      this.queriesSinRespuesta = this.queriesSinRespuesta > 0 ? --this.queriesSinRespuesta : 0
      this.resultadoPeticionEquipos = getMensajeResultadoPeticion(error, tipoPeticion)
    },
    resetEstadoEquiposStore() {
      this.queriesSinRespuesta = 0
      this.erroresEquiposStore = false
      this.resultadoPeticionEquipos = null
    },
    resetPeticionesEquiposStore() {
      this.queriesSinRespuesta = 0
    },
    resetErroresEquiposStore() {
      this.erroresModelosStore = false
    },
    resetResultadoPeticionEquipos() {
      this.resultadoPeticionEquipos = null
    },
    equipoToBody({ numeroSerie, fechaAdquisicion, fechaAsignacion, tipoEquipo, modelo, persona, unidad }) {
      return { numeroSerie, fechaAdquisicion, fechaAsignacion, tipoEquipo, modelo, persona, unidad }
    },
    actualizaEquipoDeLista(equipo) {
      this.formateaEquipo(equipo)
      let index = this.listaEquipos.findIndex((e) => e._links.self.href == equipo._links.self.href)

      if (index > -1) {
        this.listaEquipos[index] = equipo  
      } else {
        this.listaEquipos.push(equipo)
      }
      this.equipoSeleccionado = equipo
    },
    formateaEquipo(equipo) {
      addEstadoAsignacion(equipo)
      equipo.fechaAdquisicion = stringToDate(equipo.fechaAdquisicion)
      equipo.fechaAsignacion = stringToDate(equipo.fechaAsignacion)
    },

    setPalabraBuscadaStore(palabra) {
      this.palabraBuscadaStore = palabra
    }
  },
})